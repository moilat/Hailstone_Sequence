package rp.html;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;

/**
 * An abstract class that represents a builder for {@link AbstractHtml}.
 * <br>
 * It's designed using builder pattern.
 * 
 * @author Raimondo Pastore
 *
 */
public abstract class AbstractHtmlBuilder {
	
	protected final static String TAG_HTML_OP = "<!DOCTYPE html>\n<HTML>\n";
	protected final static String TAG_HTML_CL = "</HTML>";
	protected final static String TAG_BODY_OP = "<BODY>\n";
	protected final static String TAG_BODY_CL = "</BODY>\n";
	protected final static String TAG_H1_OP = "<H1>";
	protected final static String TAG_H1_CL = "</H1>\n";
	protected final static String TAG_UL_OP = "<UL>\n";
	protected final static String TAG_UL_CL = "</UL>\n";
	protected final static String TAG_LI_OP = "<LI>";
	protected final static String TAG_LI_CL = "</LI>\n";

	private ArrayList<String> pageElements;
	
	protected String htmlSource;
	
	/**
	 * Initialize a new {@link AbstractHtmlBuilder}
	 */
	public  AbstractHtmlBuilder() {
		
		pageElements = new ArrayList<String>();
		
	}
	
	/**
	 * Adds an H1 Html element to the page.
	 * 
	 * @param element - a {@link String} that contains the text of H1 element.
	 * 
	 * @return the reference of this - {@link AbstractHtmlBuilder}.
	 */
	protected AbstractHtmlBuilder addH1(String element) {
		
		String s = new StringBuilder().append(TAG_H1_OP)
									  .append(element)
									  .append(TAG_H1_CL)
									  .toString();
		pageElements.add(s);
		return this;	
	}
	
	/**
	 * Adds an UL Html element to the page
	 * 
	 * @param elements - an array of {@link String} that contains the elements of unordered list UL.
	 * 
	 * @return the reference of this - {@link AbstractHtmlBuilder}.
	 */
	protected AbstractHtmlBuilder addUList(String... elements) {
		
		String s = new StringBuilder().append(TAG_UL_OP)
									  .toString();
		
		for(String e : elements) {
			s = s.concat(TAG_LI_OP)
				 .concat(e)
				 .concat(TAG_LI_CL);
		}
		
		s = s.concat(TAG_UL_CL);
		
		pageElements.add(s);
		return this;
	}
	
	/**
	 * Builds the Html source from the element added to the {@link AbstractHtmlBuilder} in the given order.
	 * The first time that <code>buildSource()</code> is invoked, the source code will be build 
	 * and it will be not possible to modify it anymore.
	 * 
	 * @return a {@link String} that represents the source code of the Html page.
	 */
	protected String buildSource() {
		
		if(htmlSource == null) {
			String s = new StringBuilder().append(TAG_HTML_OP)
										  .append(TAG_BODY_OP)
										  .toString();
			
			for(String e : pageElements) {
				s = s.concat(e)
					 .concat("\n");
			}
			
			
			s =  s.concat(TAG_BODY_CL)
				  .concat(TAG_HTML_CL);
			
			htmlSource = s;
		}
		
		return htmlSource;
	}
	
	
	
	/**
	 * Abstract method that creates and returns a new {@link AbstractHtml} object.
	 * 
	 * @return a {@link AbstractHtml} object
	 */
	public abstract AbstractHtml build();
}
