package rp.html;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;

import rp.hailstone.HailstoneSequence;

/**
 * The class is the builder of {@link HailstoneHtml}.
 * <br>
 * It's designed using builder pattern.
 * <br>
 * A concrete class of {@link AbstractHtmlBuilder}
 * 
 * @author Raimondo Pastore
 *
 */
public final class HailstoneHtmlBuilder extends AbstractHtmlBuilder{
	
	HailstoneSequence h;
	
	/**
	 * Creates a newly {@link HailstoneHtmlBuilder} object.
	 * 
	 * @param h is a {@link HailstoneSequence}
	 */
	public HailstoneHtmlBuilder (HailstoneSequence h) {
		
		super();
		this.h = h;
		
	}


	/** 
	 * Concrete method that creates a newly instance of {@link HailstoneHtml}
	 */
	public HailstoneHtml build() {
		
    	String h1 = "Hailstone Sequence of " +  h.getSeed();
    	String[] array = new String[3];
    	array[0] = "The input number:  <B>" + h.getSeed() + "</B>";
    	array[1] = "The number of steps it took to reach 1: <B>" +  h.getStep() + "</B>";
    	array[2] = "Second largest number in the sequence: <B>" +  h.getSecond() + "</B>";

    	this.htmlSource = this.addH1(h1)
    						  .addUList(array)
    						  .buildSource();
    	

    	return new HailstoneHtml(this);
		
	}

	/**
	 * Returns a {@link HailstoneSequence} 
	 * 
	 * @return {@link HailstoneSequence} 
	 */
	public HailstoneSequence getH() {
		
		return h;
	}


}
