package rp.html;

import java.io.File;
import java.io.IOException;

/**
 * Abstract class that represents an HTML page. 
 * It's designed to be immutable.
 * 
 * @author ghostray
 *
 */
public abstract class AbstractHtml {
	
	protected final String htmlSource;
	
	/**
	 * The costructor of the class initializes a new AbstractHtml 
	 * from an AbstractHtmlBuilder.
	 * The entire building logic is wrapped in AbstractHtmlBuilder.
	 * 
	 * @param builder is an {@link AbstractHtmlBuilder} that provides to build the {@link AbstractHtml}
	 */
	public AbstractHtml(AbstractHtmlBuilder builder) {
		
		this.htmlSource = builder.htmlSource;
	}
	
	/**
	 * Abstract method is provided to save the Html source code in an HTML page in a given directory.
	 * 
	 * @param  directory - {@link File} that represents the directory where the HTML file has to be saved
	 * @return {@link File} object that represents the HTML page
	 * 
	 * @throws IOException will throw if the given directory path is not correct
	 */
	abstract public File saveHtml(File directory) throws IOException ;

}
