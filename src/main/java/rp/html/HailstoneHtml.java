package rp.html;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;

import rp.hailstone.HailstoneSequence;

/**
 * Represents an Html page that shows the information of a specific {@link HailstoneSequence} object.
 *  <br>
 * It's a concrete class of {@link AbstractHtml}.
 * <br>
 * It's designed using builder pattern:
 * {@link HailstoneHtmlBuilder} is the class builder.
 * <br><br>
 * To iniziale in the right way a {@link HailstoneHtml}:
 * <br>
 * <code>HailstoneHtml html = new HailstoneHtmlBuilder(h).build();</code>
 * <br><br>
 * {@link HailstoneHtml} are constant; their values cannot be changed after they are created. 
 * Because HailstoneSequence objects are immutable they can be shared.
 * 
 * @author Raimondo Pastore
 *
 */
public final class HailstoneHtml extends AbstractHtml {
	
	final HailstoneSequence h;
	
	/**
	 * Initializes a newly created HailstoneHtml object.
	 * <br>
	 * It's advised to use method <code>build()</code> of {@link HailstoneHtmlBuilder} to initialize a new object.
	 * 
	 * @param builder - a {@link HailstoneHtmlBuilder} instance to build the HailstoneHtml object.
	 */
	public HailstoneHtml (HailstoneHtmlBuilder builder) {
		
		super(builder);
		this.h = builder.h;
	}

	/**
	 * Concrete method that implements the saving of the Html page.
	 * <br>
	 * It's not possible to choose a name for the file, only the path.
	 */
	public File saveHtml(File directory) throws IOException {
		
		//It's not possible to choose a name for the file, only the path
		File pathFile = new File(directory,"HailstoneSequence(" + h.getSeed() + ").html");
		
    	FileWriter f = new FileWriter(pathFile);

    	f.write(this.htmlSource);
    	f.close();
    	
    	return pathFile;
		
	}
	

}
