package rp.hailstone;

/**
 * The class HailstoneSequence represents a sequence of Hailstone of a specific number greater than one.
 * HailstoneSequence are constant; their values cannot be changed after they are created. 
 * Because HailstoneSequence objects are immutable they can be shared. 
 * 
 * The class has a lazy initialization. When a parameter is requested, 
 * the sequence will be validate and generate.
 * 
 * @author ghostray
 * @version 1.0
 *
 */
public final class HailstoneSequence {
	
	private final Long seed;
	private Long max;
	private Long second;
	private Long step;


	/**
	 * Initializes a newly created HailstoneSequence object so that it represents 
	 * an Hailstone sequence of an integer greater than one.
	 * 
	 * @param seed is a {@link Long} that represents a Long to generate the sequence.
	 */
	public HailstoneSequence(Long seed) {
		
		this.seed = seed;		  	
	}
	
	/**
	 * This method is used to validate the object. 
	 * A Hailstone sequence is valid only for the integer greater than one.
	 * 
	 * If the the class parameter seed is less than or equal to 1, 
	 * it will throws an {@link IllegalArgumentException}.
	 * 
	 * It used to a lazy initialization of the object.
	 */
	private void generateSeq() {
		
		if(seed <= 1) {
			throw new IllegalArgumentException("The seed value was less than or equal to 1");
		}
		
    	second = (long) 0;
    	step =(long) 0;
    	max = seed;
    	
    	Long x = seed;		
    	while (x!=1) {
    			
    		if(x%2 == 0 ) {
    			x = x/2;
    		}else {
    			x = 3*x+1;
    		}
    		
    		if(getMax() < x) {
    			second = max;
    			max = x;
    		}else if(getSecond() < x) {
    			second = x;
    		}
    		
    		step++;
    	}
					
	}

	/**
	 * Returns the seed of the sequence.
	 * 
	 * @return {@link Long} that represents the seed of the sequence
	 */
	public Long getSeed() {
		
		return seed;
	}

	/**
	 * Returns the maximum number of the sequence.
	 * 
	 * If the the class parameter seed is less than or equal to 1, 
	 * it will throws an unchecked {@link IllegalArgumentException}.
	 * 
	 * The class has a lazy initialization, so the class will be validated and 
	 * the attributes initialized at the first time the method is called.
	 * 
	 * @return a {@link Long} that represents the maximum number of the sequence
	 */
	public Long getMax() {
		
		if(max == null) {
			generateSeq();
		}
			
		return max;
	}

	/**
	 * Return the second largest number of the sequence.
	 * 
	 * If the the class parameter seed is less than or equal to 1, 
	 * it will throws an unchecked {@link IllegalArgumentException}.
	 * 
	 * The class has a lazy initialization, so the class will be validated and 
	 * the attributes initialized at the first time the method is called.
	 * 
	 * @return {@link Long} that represents the second largest number of the sequence.
	 */
	public Long getSecond() {
		
		if(second == null){
			generateSeq();
		}
			
		return second;
	}

	/**
	 * The number of steps it took to create the sequence.
	 * 
	 * If the the class parameter seed is less than or equal to 1, 
	 * it will throws an unchecked {@link IllegalArgumentException}.
	 * 
	 * The class has a lazy initialization, so the class will be validated and 
	 * the attributes initialized at the first time the method is called.
	 * 
	 * @return {@link Long} that represents the number of steps it took to create the sequence.
	 */
	public Long getStep() {
		
		if(step == null){
			generateSeq();
		}
			
		return step;
	}

		
}
