package main;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.Writer;
import java.nio.file.Path;
import java.nio.file.Paths;

import javax.swing.JFileChooser;
import javax.swing.JOptionPane;
import javax.swing.UIManager;

import rp.html.HailstoneHtml;
import rp.html.HailstoneHtmlBuilder;
import rp.hailstone.HailstoneSequence;
import rp.html.AbstractHtml;

	

public class App {
	
	static final Integer SUP_LIMIT = 1000000;
	static final Integer INF_LIMIT = 2;
	static final String INIT_STRING = "Insert an integer between 2 and 1.000.000 \n"
								+ "Press Ok and select the directory for the html file";
	
	static final String INFORMATION_TITLE = "";
	static final String INFORMATION_OK_GENERATION = "File generated at:\n";
	
	static final String ERROR_TITLE = "";
	static final String ERROR_PARAMETER_STRING = "Insert a rigth value";
	static final String ERROR_GENERATION_STRING = "Error during file generation";
	static final String ERROR_PATH_STRING = "Wrong Path";
	static final String GOOD_BYE_STRING = "Hyvää päivänjatkoa, hope to see you soon again :)";
	
	
    public static void main(String[] args ) 
    {
 
    	//Gui for the insert of the seed of the Hailstone Sequence
    	Long x = insertNumberGui();
    	
    	//Exits from the program if the user presses 'X' or 'UNDO' button 
    	if(x == null) {
    		goodByeMessage();
    		return;
    	}
    	//Create the HailstoneSequence object
    	HailstoneSequence h = new HailstoneSequence(x);
    	
    	//Gui for choosing a directory where to save the Html page
    	File directory = chooseDirectoryGui();
    	//Exits from the program if the user presses 'X' or 'UNDO' button
    	if(directory == null) {
    		goodByeMessage();
    		return;
    	}
    	//The Builder to create a HailstoneHtml object
		HailstoneHtml html = new HailstoneHtmlBuilder(h).build();
	
		//Saves the html page and shows the pathname of the page, the page and 
		//the directory where the html page is saved
		try {
			File htmlHailstone = html.saveHtml(directory);
			JOptionPane.showMessageDialog(null, INFORMATION_OK_GENERATION + htmlHailstone.getAbsolutePath(),
					INFORMATION_TITLE, JOptionPane.INFORMATION_MESSAGE);
			java.awt.Desktop.getDesktop().open(directory);
			java.awt.Desktop.getDesktop().open(htmlHailstone);
		}catch(IOException e) {
			showErrorMessage(ERROR_GENERATION_STRING, ERROR_TITLE);
		}
		
		//See you soon
		goodByeMessage();
			    	
    }
    
    /**
     * A Gui that allows the user to insert an integer between 2 and 1000000 between.
     * It will show an input dialog until the user inserts a wrong value or 
     * presses 'X' or 'UNDO' button.
     * 
     * @return a {@link Long} inserts by the user or <code>null</code> if the user presses on 'X' or 'UNDO' button.
     */
    static Long insertNumberGui() {
    	 
	   	Long x = (long)0;
	   	//This loop will be active until the user insert a number between 2 and 1000000
	   	//or press 'X' or 'UNDO' button 
	   	do{
	   		String s = JOptionPane.showInputDialog(INIT_STRING);
	   		//If the user presses 'X' or 'UNDO' button, the loop will end
	   		if (s == null) {
	   			return null;
	   		}
	   		try {
	   			x = Long.parseLong(s);
	   		}catch(NumberFormatException e){}
	   		
	   		if(x < INF_LIMIT || x > SUP_LIMIT) {
	   			showErrorMessage(ERROR_PARAMETER_STRING,ERROR_TITLE);
	   		}
	   		
	   	}while(x < INF_LIMIT || x> SUP_LIMIT);
	   	
	   	return x;        	
    }
    
    /**
     * A Gui that allows the user to select a specific directory path
     * It's only possible to select an existing directory.
     * It's not possible to create a new directory. 
     * 
     * @return a {@link File} that represents the chosen directory
     */
    static File chooseDirectoryGui() {
    	
    	UIManager.put("FileChooser.readOnly", Boolean.TRUE); 
    	JFileChooser fileChooser = new JFileChooser();
    	fileChooser.setFileSelectionMode(JFileChooser.DIRECTORIES_ONLY);
    	
    	File directory;
    	do {
	    	int n = fileChooser.showOpenDialog(null);
	    	
	    	//If the user presses 'X' or 'UNDO' button, the loop will end
	    	if(n != JFileChooser.APPROVE_OPTION) {
	    		return null;
	    	}
	    	
    		directory = fileChooser.getSelectedFile();
    		
    		if(directory != null && !directory.exists()) {
    			showErrorMessage(ERROR_PATH_STRING, ERROR_TITLE);
    		}
    		
    	}while(directory != null && !directory.exists());
    	
		return directory;	
    }
    
    /**
     * Returns an Error Message with a specific message and title.
     *   
     * @param message - represents the message of the Dialog
     * @param title - represents the title of the Dialog
     */
    static void showErrorMessage(String message, String title) {
    	
		JOptionPane.showMessageDialog(null, message,
				title, JOptionPane.ERROR_MESSAGE);	
    }
    
    /**
     * Returns a goodbye message.   
     */
    static void goodByeMessage() {
    	
		JOptionPane.showMessageDialog(null, GOOD_BYE_STRING,
				"", JOptionPane.INFORMATION_MESSAGE);	
    }


}
