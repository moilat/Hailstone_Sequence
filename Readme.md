AUTOR: Raimondo Pastore

The project is created with the use of Maven and Eclipse.

You can find the source code in the folder src.

At the path "target/Hailstone-Sequence.jar", you can find the executable jar of the application with a minimal GUI interface.

In the folder there are some examples of the output.
